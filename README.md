This repository is a collection of my solutions for the [Advent of Code 2021](http://adventofcode.com/2021) calendar.

Puzzle                                                    |
--------------------------------------------------------- |
[Day 1: Sonar Sweep](aoc/day01)                           |
[Day 2: Dive!](aoc/day02)                                 |
[Day 3: Binary Diagnostic](aoc/day03)                     |
[Day 4: Giant Squid](aoc/day04)                           |
[Day 5: Hydrothermal Venture](aoc/day05)                  |
[Day 6: Lanternfish](aoc/day06)                           |
[Day 7: The Treachery of Whales](aoc/day07)               |   
[Day 8: Seven Segment Search](aoc/day08)                  |
[Day 9: Smoke Basin](aoc/day09)                           |
[Day 10: Syntax Scoring](aoc/day10)                       |
[Day 11: Dumbo Octopus](aoc/day11)                        |
[Day 12: Passage Pathing](aoc/day12)                      |
[Day 13: Transparent Origami](aoc/day13)                  |
[Day 14: Extended Polymerization](aoc/day14)              |
[Day 15: Chiton](aoc/day15)                               |
[Day 16: Packet Decoder](aoc/day16)                       |
[Day 17: Trick Shot](aoc/day17)                           |
[Day 18: Snailfish](aoc/day18)                            |
[Day 19: Beacon Scanner](aoc/day19)                       |
[Day 20: Trench Map](aoc/day20)                           |
[Day 21: Dirac Dice](aoc/day21)                           |
[Day 22: Reactor Reboot](aoc/day22)                       |
[Day 23: Amphipod](aoc/day23)                             |
[Day 24: Arithmetic Logic Unit](aoc/day24)                |
[Day 25: Sea Cucumber](aoc/day25)                         |
