# Advent of Code - Day 16 - Part Two
from aoc.day16.bits import read_packet, parse_input, process_packets


def result(input):
    bits = parse_input(input)
    return process_packets(bits)