# Advent of Code - Day 16 - Part One
from aoc.day16.bits import read_packet, parse_input, sum_of_versions


def result(input):
    bits = parse_input(input)
    return sum_of_versions(bits)
