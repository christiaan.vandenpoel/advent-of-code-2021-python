from bitarray import bitarray
from bitarray.util import hex2ba, ba2int
from functools import reduce

def parse_input(input):
    return hex2ba(input[0])

def sum_of_versions(bits):
    return read_packet(bits)[0]

def process_packets(bits):
    return read_packet(bits)[3]

def read_packet(bits):
    initial_length = len(bits)
    version, bits = take_bits(bits, 3)
    type_id, bits = take_bits(bits, 3)
    value = int()
    if type_id == 4:
        value_ba = bitarray()
        while bits[0] == 1:
            bits = bits[1:]
            value_ba.extend(bits[0:4])
            bits = bits[4:]

        bits = bits[1:]
        value_ba.extend(bits[0:4])

        bits = bits[4:]
        value = ba2int(value_ba)
    else:
        length_type_id, bits = ba2int(bits[0:1]), bits[1:]
        values = []
        if length_type_id == 0:
            length, bits = take_bits(bits, 15)
            while length > 0:
                other_version, bits, bits_read, val = read_packet(bits)
                version += other_version
                length -= bits_read
                values.append(val)
        else:
            number_of_packets, bits = take_bits(bits, 11)
            for _ in range(number_of_packets):
                other_version, bits, bits_read, val = read_packet(bits)
                version += other_version
                values.append(val)
        if type_id == 0:
            value = sum(values)
        elif type_id == 1:
            value = reduce(lambda acc, item: acc * item, values)
        elif type_id == 2:
            value = min(values)
        elif type_id == 3:
            value = max(values)
        elif type_id == 5:
            value = 1 if values[0] > values[1] else 0
        elif type_id == 6:
            value = 1 if values[0] < values[1] else 0
        elif type_id == 7:
            value = 1 if values[0] == values[1] else 0
        else:
            raise ValueError(type_id)


    return version, bits, initial_length - len(bits), value


def take_bits(ba, count):
    result = ba2int(ba[0:count])
    ba = ba[count:]
    return result, ba
