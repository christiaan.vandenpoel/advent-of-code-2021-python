# Advent of Code - Day 19 - Part One
from aoc.day19.beacons import join, parse


def result(input):
    joined, scanners = join(parse(input))
    return len(joined)

