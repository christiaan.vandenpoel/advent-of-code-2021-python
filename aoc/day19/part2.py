# Advent of Code - Day 19 - Part Two
from itertools import combinations
from aoc.day19.beacons import join, parse

def result(input):
    joined, scanners = join(parse(input))
    maxdistance = 0
    for combination in combinations(scanners, 2):
        x1,y1,z1 = combination[0]
        x2, y2, z2 = combination[1]

        distance = abs(x2-x1) + abs(y2-y1) + abs(z2 - z1)
        maxdistance = max(maxdistance, distance)

    return maxdistance