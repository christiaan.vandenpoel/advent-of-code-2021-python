# Advent of Code - Day 8 - Part One
from pprint import pprint
from .util import parse_notes


def result(input):
    required_len = [2, 4, 3, 7]
    count = 0
    notes = parse_notes(input)
    for (_, displays) in notes:
        for display in displays:
            if len(display) in required_len:
                count += 1
    return count
