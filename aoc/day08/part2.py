# Advent of Code - Day 8 - Part Two
from collections import defaultdict
from functools import reduce
from .util import parse_notes

def wiring_for(map, value):
    for wiring, v in map.items():
        if v == value:
            return wiring


def result(input):
    notes = parse_notes(input)
    lines = list()
    for note in notes:
        solution = defaultdict(str)
        # map = dict()
        # first map wirings upon value
        wirings, displays = note[0:2]
        candidates_for_2_3_5 = list()
        candidates_for_0_6_9 = list()
        for wiring in wirings:
            wiring = frozenset(wiring)
            if len(wiring) == 2:
                solution[wiring] = '1'
            if len(wiring) == 3:
                solution[wiring] = '7'
            if len(wiring) == 4:
                solution[wiring] = '4'
            if len(wiring) == 7:
                solution[wiring] = '8'
            if len(wiring) == 5:
                candidates_for_2_3_5.append(wiring)
            if len(wiring) == 6:
                candidates_for_0_6_9.append(wiring)

        # find wiring for '3'
        # `3 (from 2,3,5)` is the one that contains all segments from **7**
        temp = list()
        for candidate in candidates_for_2_3_5:
            if set(wiring_for(solution, '7')).issubset(candidate):
                solution[candidate] = '3'
            else:
                temp.append(candidate)
        candidates_for_2_3_5 = temp

        # find wiring for '9'
        # `9 (from 0,6,9)` is the one that contains all segments from **3**
        temp = list()
        for candidate in candidates_for_0_6_9:
            if set(wiring_for(solution, '3')).issubset(candidate):
                solution[candidate] = '9'
            else:
                temp.append(candidate)
        candidates_for_0_6_9 = temp

        # find wiring for '0'
        # `0 (from 0,6,-)` is the one that contains all segments from **7**
        temp = list()
        for candidate in candidates_for_0_6_9:
            if set(wiring_for(solution, '7')).issubset(candidate):
                solution[candidate] = '0'
            else:
                temp.append(candidate)
        candidates_for_0_6_9 = temp

        # find wiring for '6'
        # `6 (from -,6,-)` is automatically solved then
        assert len(candidates_for_0_6_9) == 1
        solution[candidates_for_0_6_9[0]] = '6'

        # find wiring for '5'
        # `5 (from 2,-,5)` is the one that differs by 1 segment from **6**
        temp = list()
        for candidate in candidates_for_2_3_5:
            if len(set(wiring_for(solution, '6')).difference(candidate)) == 1:
                solution[candidate] = '5'
            else:
                temp.append(candidate)
        candidates_for_2_3_5 = temp

        # find wiring for '2'
        # `2 (from 2,-,-)` is automatically solved then
        assert len(candidates_for_2_3_5) == 1
        solution[candidates_for_2_3_5[0]] = '2'

        # second, get the values displayed
        lines.append(int(reduce(lambda x,y: x+y, [solution[frozenset(display)] for display in displays])))
            
    return sum(lines)
