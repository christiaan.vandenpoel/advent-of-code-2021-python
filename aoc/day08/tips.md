## how to solve it

- `1, 4, 7, 9` can be solved directly (by their length)
- `3 (from 2,3,5)` is the one that contains all segments from **7**
- `9 (from 0,6,9)` is the one that contains all segments from **3**
- `0 (from 0,6,-)` is the one that contains all segments from **7**
- `6 (from -,6,-)` is automatically solved then
- `5 (from 2,-,5)` is the one that differs by 1 segment from **6**
- `2 (from 2,-,-)` is automatically solved then