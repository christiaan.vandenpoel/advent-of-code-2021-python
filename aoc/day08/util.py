def parse_notes(input):
    return [
            (row.split(' | ')[0].split(' '),
             row.split(' | ')[1].split(' '))
        for row in input]
