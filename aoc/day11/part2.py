# Advent of Code - Day 11 - Part Two
from pprint import pprint

from aoc.day11.util import process_grid, parse_grid, print_grid


def result(input):
    count_days = 0
    grid = parse_grid(input)
    while not all_flashed(grid):
        count_days += 1
        process_grid(grid)

    return count_days


def all_flashed(grid):
    return all(map(lambda point: grid[point] == 0, grid.keys()))
