# Advent of Code - Day 11 - Part One
from aoc.day11.util import process_grid, parse_grid


def result(input, days):
    grid = parse_grid(input)
    total_flashed = 0
    for step in range(1, days + 1):
        octopi_that_have_flashed_already = process_grid(grid)
        total_flashed += len(octopi_that_have_flashed_already)

    return total_flashed

