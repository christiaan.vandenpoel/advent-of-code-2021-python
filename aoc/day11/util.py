from pprint import pprint

from colorama import Fore, Style
from math import sqrt


def process_grid(grid):
    flashings = []
    for point, octopus in grid.items():
        energy = grid[point]
        if energy == 9:
            grid[point] = 0
            flashings.append(point)
        else:
            grid[point] += 1
    # 2. each flashings octopus increases the energy of the nearby octopi
    octopusses_flashed_today = set(flashings)

    while len(flashings) > 0:
        flash = flashings.pop()
        octopusses_flashed_today.add(flash)
        neighbours = get_neighbours_for(flash)
        for neighbour in neighbours:
            if neighbour not in grid.keys() or \
                    neighbour in octopusses_flashed_today:
                continue

            energy = grid[neighbour]

            if energy == 0:
                continue

            if energy == 9:
                energy = 0
                flashings.append(neighbour)
            else:
                energy += 1

            grid[neighbour] = energy

    return octopusses_flashed_today


def get_neighbours_for(point):
    return [(x, y)
            for x in range(point[0] - 1, point[0] + 2)
            for y in range(point[1] - 1, point[1] + 2)
            if (x, y) != point
            ]


def parse_grid(input):
    grid = dict()
    for x, row in enumerate(input):
        for y, col in enumerate(row):
            grid[(x, y)] = int(col)
    return grid


def print_grid(grid, steps, highlight=None, title=None):
    width = int(sqrt(len(grid)))
    height = width

    if title is not None:
        print(Fore.GREEN + Style.BRIGHT + title + Style.RESET_ALL)
    elif steps == 0:
        print("Start:")
    else:
        print("After step {}:".format(steps))

    for x in range(0, width):
        line = ""
        for y in range(0, height):
            elem = grid[(x, y)]
            if highlight is not None and (x, y) == highlight:
                line += Style.BRIGHT + Fore.RED + str(elem) + Style.RESET_ALL
            elif elem == 0:
                line += Style.BRIGHT + str(elem) + Style.RESET_ALL
            else:
                line += str(elem)
        print(line)
