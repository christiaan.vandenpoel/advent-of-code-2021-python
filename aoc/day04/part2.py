# Advent of Code - Day 4 - Part Two
import numpy as np
from . import bingo

def result(input):
    return bingo.play(input, -1)
