# Advent of Code - Day 4 - Part One
from . import bingo

def result(input):
    return bingo.play(input, 0)
