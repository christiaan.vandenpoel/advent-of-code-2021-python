import numpy as np

def play(input, idx_to_return):
    parts = "\n".join(input).split("\n\n")
    draws = [int(x) for x in parts[0].split(',')]
    boards = []

    create_boards_from_input(boards, parts)
    board_indexes_won = []

    # play
    for number_drawn in draws:
        for idx, board in enumerate(boards):
            if any(x[0] == idx for x in board_indexes_won):
                continue
            for v in board.flat:
                if v.value == number_drawn:
                    v.checked = True
            # now check whether we have a row or column with all checks
            if check_if_winner(board):
                board_indexes_won.append((idx, number_drawn))

    board_index_won = board_indexes_won[idx_to_return]
    index_won = board_index_won[0]
    won_by_number_drawn = board_index_won[1]
    board = boards[index_won]

    unchecked_values = map(lambda bv: bv.value, filter(lambda bv: not bv.checked, board.flat))
    total = sum(unchecked_values)
    return total * won_by_number_drawn

def check_if_winner(board):
    lines_to_check = [row for row in board]
    lines_to_check += [col for col in board.T]

    for line in lines_to_check:
        if all(map(lambda x: x.checked, line)):
            return True

    return False

def create_boards_from_input(boards, parts):
    # create the boards from the input
    for board_input in parts[1:]:
        rows = board_input.split("\n")

        numbers = [BingoValue(x) for row in rows for x in row.split()]

        nbr_of_rows = len(rows)
        nbr_of_cols = int(len(numbers) / nbr_of_rows)

        board = np.array(numbers).reshape(nbr_of_cols, nbr_of_rows)
        boards.append(board)

class BingoValue:
    value: int
    checked: bool

    def __init__(self, value):
        self.value = int(value)
        self.checked = False

    def __repr__(self):
        return "BV({0}, {1})".format(self.value, self.checked)