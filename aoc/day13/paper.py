# from collections import defaultdict
from pprint import pprint
from colorama import Fore, Style


def parse_paper(lines):
    paper = dict()
    for line in lines:
        point = line.split(',')
        x = int(point[0])
        y = int(point[1])
        paper[(x,y)] = '#'
    return paper

def fold_paper(paper, around, value):
    new_paper = dict()
    for point in paper.keys():
        x,y = point
        if around == 'y':
            if y <= value - 1:
                new_paper[(x,y)] = paper[point]
            else:
                y = value - 1 - (y - value - 1)
                new_paper[(x,y)] = paper[point]
        else:
            if x <= value - 1:
                new_paper[(x,y)] = paper[point]
            else:
                x = value - 1 - (x - value - 1)
                new_paper[(x,y)] = paper[point]

    return new_paper


def print_paper(paper, title=None):
    x_min = 0
    y_min = 0
    x_max = 0
    y_max = 0

    for key in paper.keys():
        x_max = max(x_max, key[0])
        y_max = max(y_max, key[1])
        x_min = min(x_min, key[0])
        y_min = min(y_min, key[1])

    # pprint(((x_min, y_min),(x_max, y_max)))

    if title is not None:
        print(Fore.GREEN + title + Style.RESET_ALL)
    for y in range(y_min, y_max + 1):
        line = ""
        for x in range(x_min, x_max + 1):
            # v = paper[(x,y)]
            if (x,y) in paper:
                line += Fore.WHITE + Style.BRIGHT + paper[(x,y)] + Style.RESET_ALL
            else:
                line += '.'
        print(line)

