# Advent of Code - Day 13 - Part Two
from aoc.day13.paper import parse_paper, print_paper, fold_paper


def result(input):
    grid, folds = "\n".join(input).split("\n\n")[0:2]
    grid = grid.splitlines()
    folds = folds.splitlines()
    paper = parse_paper(grid)
    print_paper(paper, title="Start")

    for idx, fold in enumerate(folds):
        around, value = fold.split("=")[0:2]
        around = around[-1]
        value = int(value)
        paper = fold_paper(paper, around, value)

        print_paper(paper, title="\nFold {}, around '{}' for value {}".format(idx + 1, around, value))

    return len(list(filter(lambda k: paper[k] == '#', paper.keys())))
