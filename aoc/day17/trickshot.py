from itertools import accumulate


def accumulator(acc, item):
    return acc + item


def y_stepper(step):
    yield step
    while True:
        step -= 1
        yield step


def x_stepper(step):
    yield step
    while step > 0:
        step -= 1
        yield step
    while True:
        yield 0

def possible_velocities(x_range, y_range):
    x_min, x_max = x_range
    y_min, y_max = y_range
    pos_velocities = list()
    for xvel in range(0, 300):
        for yvel in range(y_min, 300):
            max_y = 0
            zipper = zip(accumulate(x_stepper(xvel), accumulator), accumulate(y_stepper(yvel), accumulator))
            for (x, y) in zipper:
                max_y = max(max_y, y)
                if x_min <= x <= x_max and y_max >= y >= y_min:
                    pos_velocities.append(((xvel, yvel), max_y))
                    break
                if x > x_max or y <= y_min:
                    break
    return pos_velocities
