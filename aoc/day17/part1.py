# Advent of Code - Day 17 - Part One
from pprint import pprint

from aoc.day17.trickshot import possible_velocities


def result(input):
    x_range, y_range = map(lambda v: v.split("=")[1], input[0].replace("target area: ", "").split(", "))
    x_range = list(map(lambda v: int(v), x_range.split("..")))
    y_range = list(map(lambda v: int(v), y_range.split("..")))

    pos_velocities = possible_velocities(x_range, y_range)
    pos_velocities.sort(key=lambda x: x[1])

    return pos_velocities[-1][1] if len(pos_velocities) > 0 else None




