from functools import reduce


def calculate_inputs(row):
    corrupted_scores = {
        ")": 3,
        "]": 57,
        "}": 1197,
        ">": 25137
    }
    incomplete_scores = {
        "{": 3,
        "[": 2,
        "(": 1,
        "<": 4
    }
    stack = list()

    for char in row:
        if char in "({[<":
            stack.append(char)
        else:
            previous_char = stack.pop()
            if (previous_char == "(" and char != ")")\
                    or (previous_char == "{" and char != "}")\
                    or (previous_char == "[" and char != "]")\
                    or (previous_char == "<" and char != ">"):
                return ("corrupted" ,corrupted_scores[char])

    if len(stack) > 0:
        stack.reverse()
        score = reduce(lambda acc, char: acc * 5 + incomplete_scores[char], stack, 0)
        return ("incomplete", score)

    return ("correct", 0)
