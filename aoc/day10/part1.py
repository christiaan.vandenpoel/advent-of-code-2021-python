# Advent of Code - Day 10 - Part One
from aoc.day10.calculator import calculate_inputs


def result(input):
    scores = map(lambda row: calculate_inputs(row), input)
    scores = list(filter(lambda item: item[0] == 'corrupted', scores))
    scores = map(lambda score: score[1], scores)
    return sum(scores)

