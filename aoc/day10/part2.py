# Advent of Code - Day 10 - Part Two
from functools import reduce
from aoc.day10.calculator import calculate_inputs


def result(input):
    scores = map(lambda row: calculate_inputs(row), input)
    scores = list(filter(lambda item: item[0] == 'incomplete', scores))
    scores = list(map(lambda score: score[1], scores))
    scores.sort()
    return scores[int(len(scores)/2)]


# def calculate_incomplete(row):
#     scores = {
#         "{": 3,
#         "[": 2,
#         "(": 1,
#         "<": 4
#     }
#     stack = list()
#
#     for char in row:
#         if char in "({[<":
#             stack.append(char)
#         else:
#             previous_char = stack.pop()
#             if (previous_char == "(" and char != ")")\
#                     or (previous_char == "{" and char != "}")\
#                     or (previous_char == "[" and char != "]")\
#                     or (previous_char == "<" and char != ">"):
#                 return 0
#
#     stack.reverse()
#     score = reduce(lambda acc, char: acc * 5 + scores[char], stack, 0)
#     return score
