# Advent of Code - Day 9 - Part Two
from aoc.day09.util import get_lowest_points, create_cave, get_neighbours
from functools import reduce
from operator import itemgetter



def result(input):
    width = len(input[0])
    height = len(input)

    cave = create_cave(height, input, width)
    lowest = get_lowest_points(cave, height, width)
    basins = list()

    for basin_lowest_point in lowest:
        points_in_basin = list()
        stack = list()
        visited = set()

        points_in_basin.append(basin_lowest_point)
        stack.extend(get_neighbours(basin_lowest_point, height, width))
        visited = visited.union(get_neighbours(basin_lowest_point, height, width))
        visited.add(basin_lowest_point)

        while len(stack) > 0:
            point_to_check = stack.pop()
            value_for_point = cave[point_to_check]
            if value_for_point < 9:
                points_in_basin.append(point_to_check)
                neighbours = get_neighbours(point_to_check, height, width)
                stack.extend([x for x in get_neighbours(point_to_check, height, width) if x not in visited])
                visited = visited.union(neighbours)

        size_of_basin = len(points_in_basin)
        basins.append((points_in_basin, size_of_basin))

    basins.sort(key=itemgetter(1), reverse=True)
    return reduce(lambda x,y: x*y, map(lambda t: t[1],basins[0:3]))



