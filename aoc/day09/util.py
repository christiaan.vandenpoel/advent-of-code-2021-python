import numpy as np


def create_cave(height, input, width):
    cave = np.array([int(x) for x in "".join(input)]).reshape(height, width)
    return cave

def get_lowest_points(cave, height, width):
    lowest = list()
    for coordinate in [(x, y) for x in range(0, height) for y in range(0, width)]:
        value_at_coordinate = cave[coordinate[0], coordinate[1]]

        if all(map(lambda v: value_at_coordinate < v,
                   [cave[c[0], c[1]] for c in get_neighbours(coordinate, height, width)])):
            lowest.append(coordinate)
    return lowest

def get_neighbours(coordinate, height, width):
    x,y = coordinate
    if x - 1 >= 0:
        yield (x-1,y)
    if y - 1 >= 0:
        yield (x, y-1)
    if x + 1 < height:
        yield(x+1,y)
    if y + 1 < width:
        yield(x,y+1)