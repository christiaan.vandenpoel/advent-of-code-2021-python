# Advent of Code - Day 9 - Part One
from pprint import pprint
from aoc.day09.util import get_lowest_points, create_cave


def result(input):
    width = len(input[0])
    height = len(input)

    cave = create_cave(height, input, width)
    lowest = get_lowest_points(cave, height, width)

    return sum(map(lambda loc: cave[loc[0], loc[1]] + 1, lowest))


