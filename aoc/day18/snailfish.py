from functools import reduce
import math


def parse(input):
    lines = list(map(lambda l: parse_line(l), input))
    return lines

def parse_line(line):
    result = []
    idx = 0
    current_number = []
    while idx < len(line):
        if line[idx] in '[]':
            if line[idx] == ']':
                if len(current_number) > 0:
                    result.append(int("".join(current_number)))
                    current_number = []
            result.append(line[idx])
        elif line[idx] == ',':
            if len(current_number) > 0:
                result.append(int("".join(current_number)))
                current_number = []
        else:
            current_number.append(line[idx])
        idx += 1

    return result

def add_to_first_right(line, from_idx, value):
    idx = from_idx + 1
    while idx < len(line):
        if line[idx] != '[' and line[idx] != ']':
            line[idx] += value
            break
        idx += 1
    return line

def add_to_first_left(line, from_idx, value):
    idx = from_idx - 1
    while idx >= 0:
        if line[idx] != '[' and line[idx] != ']':
            line[idx] += value
            break
        idx -= 1
    return line

def split(line):
    idx = 0
    while idx < len(line):
        if line[idx] == '[' or line[idx] == ']':
            pass
        elif line[idx] >= 10:
            line = line[:idx] + ['[', line[idx] // 2, math.ceil(line[idx] / 2), ']'] + line[idx+1:]
            return line, "splitted"
        idx += 1
    return line, "nothing"

def explode(line):
    idx = 0
    count_parenthesis = 0
    while idx < len(line):
        if line[idx] == '[':
            count_parenthesis += 1
        elif line[idx] == ']':
            count_parenthesis -= 1
        else:
            # it is a number
            if line[idx + 1] != ']' and count_parenthesis > 4:

                # add the first left with the left part of this pair
                # add the first right with the right part of this pair
                line = add_to_first_right(line, idx + 1, line[idx + 1])
                line = add_to_first_left(line, idx - 1, line[idx])

                # replace this pair with '0'
                line = line[:idx - 1] + [0] + line[idx + 3:]

                return line, "exploded"
            else:
                pass
        idx += 1

    return line, "nothing"

def magnitude(summation):
    while True:
        changed = False
        idx = 0
        if len(summation) == 1:
            return summation[0]
        while idx < len(summation):
            item = summation[idx]
            if item != '[' and item != ']':
                if summation[idx+1] != ']' and summation[idx+1] != '[':
                    magnitude = 3*item + 2*summation[idx+1]
                    summation = summation[:idx-1] + [magnitude] + summation[idx+3:]
                    changed = True
                elif summation[idx+1] == ']':
                    summation = summation[:idx-1] + [item] + summation[idx:]
                    changed = True
            idx += 1
        if not changed:
            break;

    return summation

def solve_reducer(acc, item):
    solution = ['['] + acc + item + [']']
    while True:
        solution, action = explode(solution)
        if action == "nothing":
            solution, action = split(solution)
            if action == "nothing":
                return solution
    return solution

def solve(input):
    lines = parse(input)
    solution = solve_lines(lines)
    return solution


def solve_lines(lines):
    return reduce(solve_reducer, lines)
