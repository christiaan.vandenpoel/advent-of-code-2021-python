# Advent of Code - Day 18 - Part One


from aoc.day18.snailfish import solve, magnitude


def result(input):
    solution = solve(input)
    return magnitude(solution)


