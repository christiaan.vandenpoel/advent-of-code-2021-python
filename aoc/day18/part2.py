# Advent of Code - Day 18 - Part Two
from aoc.day18 import snailfish
from itertools import permutations

def result(input):
    lines = snailfish.parse(input)
    maximum = 0
    for comb in permutations(lines, 2):
        magnitude = snailfish.magnitude(snailfish.solve_lines(comb))
        if magnitude > maximum:
            maximum = magnitude

    return maximum
