# Advent of Code - Day 15 - Part One
from pprint import pprint
from colorama import Fore, Style

from aoc.day15.util import parse_input, print_cave, find_shortest_path, path


def result(input):
    cave_height = len(input)
    cave_width = len(input[0])

    cave = parse_input(input)
    print_cave(cave, cave_width, cave_height)

    start = (0, 0)
    goal = (cave_width - 1, cave_height - 1)
    steps = find_shortest_path(cave, cave_height, cave_width, goal, start)

    print_cave(cave, cave_width, cave_height, path(goal, steps))
    return sum(cave[(x, y)] for (x, y) in path(goal, steps)) - cave[start]

