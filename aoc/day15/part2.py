# Advent of Code - Day 15 - Part Two
from aoc.day15.util import parse_input, print_cave, find_shortest_path, path, expand


def result(input):
    cave_height = len(input)
    cave_width = len(input[0])

    cave = expand(parse_input(input), 5, cave_width, cave_height)
    cave_width *= 5
    cave_height *= 5

    start = (0, 0)
    goal = (cave_width - 1, cave_height - 1)
    steps = find_shortest_path(cave, cave_height, cave_width, goal, start)

    return sum(cave[(x, y)] for (x, y) in path(goal, steps)) - cave[start]
