from colorama import Fore, Style


def find_shortest_path(cave, cave_height, cave_width, goal, start):
    paths = [(0, (0, 0))]
    steps = {start: None}
    costs = {start: 0}
    cnt = 0
    while True:
        _, (x, y) = paths.pop()

        if (x, y) == goal:
            break

        for neighbor in get_neighbours_for((x, y), cave_width, cave_height):
            cost = costs[(x, y)] + cave[(x, y)]
            if neighbor not in costs or cost < costs[neighbor]:
                costs[neighbor] = cost
                paths.append((cost, neighbor))
                # poor man's priority queue
                paths.sort(reverse=True)
                steps[neighbor] = (x, y)
    return steps


def parse_input(input):
    cave = dict()
    for y, x in enumerate(input):
        for x, value in enumerate(x):
            cave[x, y] = int(value)
    return cave


def all_paths_end(paths, width, height):
    return any(map(lambda p: p[-1] == (width - 1, height - 1), paths))


def print_cave(cave, width, height, path=None):
    print()
    for y in range(0, height):
        line = ""
        for x in range(0, width):
            if path is not None and (x, y) in path:
                line += Fore.GREEN + Style.BRIGHT + str(cave[x, y]) + Style.RESET_ALL
            else:
                line += str(cave[x, y])
        print(line)


def path(loc, steps):
    path = [loc]
    step = steps[loc]
    while step:
        path.append(step)
        step = steps[step]
    return path


def get_neighbours_for(last_point, cave_width, cave_height):
    px, py = last_point
    n = []
    if px - 1 >= 0:
        n.append((px - 1, py))
    if py - 1 >= 0:
        n.append((px, py - 1))
    if px + 1 < cave_width:
        n.append((px + 1, py))
    if py + 1 < cave_height:
        n.append((px, py + 1))
    return n

def expand(cave, count, width, height):
    expanded_cave = dict()

    for y in range(0, height * count):
        for x in range(0, width * count):
            p = (x, y)
            if p in cave:
                expanded_cave[p] = cave[p]
            else:
                orig_point = (x % width, y % height)
                value = cave[orig_point] + x // width + y // width
                if value > 9:
                    value -= 9
                expanded_cave[p] = value
    return expanded_cave
