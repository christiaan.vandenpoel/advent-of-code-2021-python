# Advent of Code - Day 21 - Part One
from itertools import cycle, islice


def result(input):
    p1, p2 = (list(map(lambda l: int(l[-1]), input)))
    players = [
        (1, p1, 0),
        (2, p2, 0)
    ]



    dice_iterator = iter(cycle(dice()))

    dice_count = 0
    losing_player_points = 0

    while True:
        plyr_idx, pos, score = players.pop(0)

        dice_rolls = [next(dice_iterator),
                      next(dice_iterator),
                      next(dice_iterator)]
        dice_count += len(dice_rolls)
        new_pos = roundrobin(pos + sum(dice_rolls), 10)
        new_score = score + new_pos
        print("Player {} rolls '{}' and moves to space {} for a total score of {}."
              .format(plyr_idx, "+".join(map(str, dice_rolls)), new_pos, new_score))
        players.append((plyr_idx, new_pos, new_score))
        if new_score >= 1000:
            losing_player_points = players[0][2]
            break

    return dice_count * losing_player_points


def dice():
    for x in range(1, 101):
        yield x

def roundrobin(v, maximum):
    if v < maximum:
        return v
    if v % maximum == 0:
        return maximum
    return v % maximum
