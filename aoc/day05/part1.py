# Advent of Code - Day 5 - Part One
from . import vents

def result(input):
    return vents.count_overlaps(input=input, miminum=2, include_diagonal=False)
