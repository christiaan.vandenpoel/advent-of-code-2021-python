import re


def count_overlaps(input, miminum, include_diagonal):
    floor = {}

    p = re.compile(r'(?P<x1>\d+),(?P<y1>\d+) -> (?P<x2>\d+),(?P<y2>\d+)')

    for line in input:
        m = p.match(line)
        x1 = int(m.group('x1'))
        y1 = int(m.group('y1'))
        x2 = int(m.group('x2'))
        y2 = int(m.group('y2'))

        x_range = range(x1, x2 + 1)
        if x2 < x1:
            x_range = range(x1, x2 - 1, -1)
        y_range = range(y1, y2 + 1)
        if y2 < y1:
            y_range = range(y1, y2 - 1, -1)

        if x1 == x2 or y1 == y2:
            points = [(x, y) for x in x_range for y in y_range]
        elif include_diagonal:
            points = [(x, y) for (x, y) in zip(x_range, y_range)]
        else:
            points = []

        for point in points:
            value = floor.get(point, 0)
            floor[point] = value+1

    return len(dict(filter(lambda elem: elem[1] >= miminum, floor.items())))
