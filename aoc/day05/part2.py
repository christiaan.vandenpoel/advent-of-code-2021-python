# Advent of Code - Day 5 - Part Two
from aoc.day05 import vents


def result(input):
    return vents.count_overlaps(input=input, miminum=2, include_diagonal=True)
