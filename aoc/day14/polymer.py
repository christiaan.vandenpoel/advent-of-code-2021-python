from collections import Counter
from pprint import pprint


def parse_input(input):
    polymer_template, rules = "\n".join(input).split("\n\n")[0:2]
    rules_map = dict()
    for rule in rules.splitlines():
        f, t = rule.split(" -> ")[0:2]
        rules_map[f] = t
    return polymer_template, rules_map

def perform_insertion(polymer_template, rules):
    new_polymer = Counter()
    for pair in polymer_template:
        if pair in rules:
            np1 = pair[0] + rules[pair]
            np2 = rules[pair] + pair[1]
            new_polymer[np1] = new_polymer[np1] + polymer_template[pair]
            new_polymer[np2] = new_polymer[np2] + polymer_template[pair]
    return new_polymer

    idx = 0
    while True:
        if idx >= len(polymer_template):
            return polymer_template
        substring = polymer_template[idx:idx +2]
        if substring in rules:
            polymer_template = polymer_template[0:idx + 1] + rules[substring] + polymer_template[idx + 1:]
            idx += 1

        idx += 1
