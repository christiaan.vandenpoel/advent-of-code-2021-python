# Advent of Code - Day 14 - Part Two
import math
from collections import Counter, defaultdict

from aoc.day14.polymer import perform_insertion, parse_input


def result(input):
    polymer_template, rules_map = parse_input(input)

    pairs = list(map(lambda x: x[0]+x[1], zip(polymer_template[:-1], polymer_template[1:])))
    polymer_template = Counter(pairs)

    for step in range(1, 41):
        polymer_template = perform_insertion(polymer_template, rules_map)

    c = defaultdict(int)
    for key in polymer_template:
        for char in key:
            c[char] += polymer_template[key]
    for key, value in c.items():
        c[key] = math.ceil(value / 2)
    counter = c

    return max(counter.values())-min(counter.values())