# Advent of Code - Day 3 - Part Two

def result(input):
    oxygen_generator_rating = 0
    co2_scrubber_rating = 0

    rows = input
    idx = 0
    while(len(rows) > 1):
        zeros_for_idx = []
        ones_for_idx = []

        for row in rows:
            if row[idx] == '1':
                ones_for_idx.append(row)
            else:
                zeros_for_idx.append(row)

        if len(ones_for_idx) > len(zeros_for_idx) or len(ones_for_idx) == len(zeros_for_idx):
            rows = ones_for_idx
        else:
            rows = zeros_for_idx

        idx += 1

    oxygen_generator_rating = int(rows[0], 2)

    rows = input
    idx = 0
    while(len(rows) > 1):
        zeros_for_idx = []
        ones_for_idx = []

        for row in rows:
            if row[idx] == '1':
                ones_for_idx.append(row)
            else:
                zeros_for_idx.append(row)

        if len(ones_for_idx) < len(zeros_for_idx):
            rows = ones_for_idx
        else:
            rows = zeros_for_idx

        idx += 1

    co2_scrubber_rating = int(rows[0], 2)

    return oxygen_generator_rating * co2_scrubber_rating
