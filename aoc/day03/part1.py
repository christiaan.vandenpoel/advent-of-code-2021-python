# Advent of Code - Day 3 - Part One

def result(input):
    gamma = 0
    epsilon = 0

    nbr_of_digits = len(input[0])

    for idx in range(0,nbr_of_digits):
        gamma <<= 1
        epsilon <<= 1

        count_of_zeros = 0
        count_of_ones = 0

        for row in input:
            if row[idx] == '0':
                count_of_zeros += 1
            else:
                count_of_ones += 1

        if count_of_ones > count_of_zeros:
            gamma += 1
        elif count_of_zeros > count_of_ones:
            epsilon += 1

    return gamma * epsilon
