# Advent of Code - Day 1 - Part Two
from pprint import pprint
from more_itertools import triplewise

def result(input):
    positive_changes = 0

    int_list = [int(x) for x in input]
    windows = [sum(x) for x in triplewise(int_list)]

    for idx, item in enumerate(windows):
        if idx > 0:
            if item > windows[idx-1]:
                positive_changes += 1

    return positive_changes
