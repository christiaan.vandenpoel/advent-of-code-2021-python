# Advent of Code - Day 1 - Part One

def result(input):
    int_list = [int(x) for x in input]
    return sum(map(lambda elem: 1 if elem[1] > elem[0] else 0, zip(int_list, int_list[1:])))
