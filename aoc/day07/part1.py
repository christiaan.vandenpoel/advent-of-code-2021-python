# Advent of Code - Day 7 - Part One
from functools import reduce


def result(input):
    crab_positions = [int(cp) for cp in input[0].split(",")]
    crab_positions.sort()
    min, *_, max = crab_positions
    minimum = None
    for position in range(min, max+1):
        total_diff = reduce(lambda acc, elem: acc + abs(position - elem), crab_positions, 0)
        if minimum is None or total_diff < minimum:
            minimum = total_diff
    return minimum
