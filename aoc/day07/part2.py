# Advent of Code - Day 7 - Part Two
from functools import reduce, cache


def result(input):
    crab_positions = [int(cp) for cp in input[0].split(",")]
    crab_positions.sort()
    min, *_, max = crab_positions
    minimum = None
    for position in range(min, max+1):
        total_diff = reduce(lambda acc, elem: acc + increase_costs(abs(position - elem)), crab_positions, 0)
        if minimum is None or total_diff < minimum:
            minimum = total_diff
    return minimum

@cache
def increase_costs(total):
    return sum((range(1, total + 1)))
