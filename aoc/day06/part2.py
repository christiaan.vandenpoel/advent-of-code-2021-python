# Advent of Code - Day 6 - Part Two
from .lanternfish import evolve


def result(input, days):
    return evolve(input, days)
