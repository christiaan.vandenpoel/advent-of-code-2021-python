# Advent of Code - Day 6 - Part One
from .lanternfish import evolve


def result(input, days):
    return evolve(input, days)
    population = [int(x) for x in input[0].split(',')]

