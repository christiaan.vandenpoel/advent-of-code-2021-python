from collections import Counter

def evolve(input, days):
    population = [int(x) for x in input[0].split(',')]
    # index: day of the cycle (0..8)
    # variable: number of lanternfish in this day of the cycle
    p = [0] * 9
    for k, v in Counter(population).items():
        p[k] = v

    # n: iterations / days to simulate
    for i in range(days):
        q = p[1:]  # shift whole popluation
        q.append(p[0])  # new lanternfish
        q[6] += p[0]  # reenter 7 day cycle
        p = q
    return sum(p)  # total number of lanternfish after n days