# Advent of Code - Day 20 - Part One
from collections import defaultdict
from functools import reduce
from colorama import Fore, Style

# TODO: find out why it doesn't work
# tried but incorrect: 5393, 5438
def result(input):
    enhancement_algo = input[0]

    image = parse_image(input)

    print_image(image, "Initial image")
    for x in range(2):
        image = enhance_image(image, enhancement_algo, (x % 2) == 1)
        print_image(image, "After iteration {}".format(x + 1))

    length = 0
    for v in image.values():
        if v == '#':
            length += 1

    return length


def parse_image(input):
    image = defaultdict(lambda: '.')
    for y, line in enumerate(input[2:]):
        for x, char in enumerate(line):
            image[(x, y)] = char
    return image


def enhance_image(image, enhancement_algo, first):
    new_image = defaultdict(lambda: '#') if first and enhancement_algo[0] == '#' else defaultdict(lambda: '.')
    (x_min, x_max), (y_min, y_max) = get_image_bounds(image)

    from_x = x_min - 1
    to_x = x_max + 2
    from_y = y_min - 1
    to_y = y_max + 2

    for x in range(from_x, to_x):
        for y in range(from_y, to_y):
            point = (x, y)
            value = reduce(lambda acc, p: acc << 1 | (0x01 if image[p] == '#' else 0), points_for(point), 0)
            new_image[point] = enhancement_algo[value]


    return new_image


def points_for(point):
    x, y = point
    yield x - 1, y - 1
    yield x, y - 1
    yield x + 1, y - 1
    yield x - 1, y
    yield x, y
    yield x + 1, y
    yield x - 1, y + 1
    yield x, y + 1
    yield x + 1, y + 1


def print_image(image, title=None, point=None):
    (x_min, x_max), (y_min, y_max) = get_image_bounds(image)

    if title is not None:
        print(title)
    for y in range(y_min, y_max + 1):
        line = ""
        for x in range(x_min, x_max + 1):
            if (x,y) == point:
                line += (Fore.GREEN + Style.BRIGHT + 'X' + Style.RESET_ALL)
            else:
                line += (Fore.WHITE + Style.BRIGHT + '#' + Style.RESET_ALL) if image.get((x, y)) == '#' else '.'
        print(line)


def get_image_bounds(image):
    x_min = 0
    x_max = 0
    y_min = 0
    y_max = 0
    for x, y in image.keys():
        x_min = min(x_min, x)
        x_max = max(x_max, x)
        y_min = min(y_min, y)
        y_max = max(y_max, y)
    return (x_min, x_max), (y_min, y_max)
