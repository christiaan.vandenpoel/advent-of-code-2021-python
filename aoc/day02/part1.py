# Advent of Code - Day 2 - Part One

def result(input):
    horizontal,vertical = 0,0

    for row in input:
        change = int(row.split(' ')[1])

        if row.startswith('forward'):
            horizontal += change
        elif row.startswith('down'):
            vertical += change
        elif row.startswith('up'):
            vertical -= change    

    return horizontal * vertical
