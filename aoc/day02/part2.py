# Advent of Code - Day 2 - Part Two

def result(input):
    horizontal,vertical,aim = 0,0,0

    for row in input:
        change = int(row.split(' ')[1])

        if row.startswith('forward'):
            horizontal += change
            vertical += (aim * change)
        elif row.startswith('down'):
            aim += change
        elif row.startswith('up'):
            aim -= change

    return horizontal * vertical
