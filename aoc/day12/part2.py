# Advent of Code - Day 12 - Part Two
from aoc.day12.util import all_paths


def result(input):
    return len(all_paths(input, alternate_logic=True))

