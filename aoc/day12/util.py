from collections import defaultdict, Counter
from pprint import pprint
from colorama import Fore, Style


def all_paths(input, alternate_logic=False):
    layout = defaultdict(list)
    for row in input:
        start, stop = row.split("-")[0:2]
        layout[start].append(stop)
        layout[stop].append(start)

    stack = list()
    stack.append(['start'])
    while not all_done(stack):
        current_path = stack.pop(0)
        if current_path[-1] == 'end':
            stack.append(current_path)
            continue
        for next_cave in layout[current_path[-1]]:
            if alternate_logic:
                if next_cave not in ['start', 'end'] \
                        and next_cave.islower() \
                        and is_visited_only_once_before(next_cave, current_path):
                    stack.append(current_path.copy() + [next_cave])
                elif next_cave == 'end':
                    stack.append(current_path.copy() + [next_cave])
                elif not next_cave.islower():
                    stack.append(current_path.copy() + [next_cave])
            else:
                if next_cave.islower() and next_cave not in current_path:
                    stack.append(current_path.copy() + [next_cave])
                elif not next_cave.islower():
                    stack.append(current_path.copy() + [next_cave])
    stack.sort()
    return stack


def is_visited_only_once_before(next_cave, previous_caves):
    if next_cave not in previous_caves:
        return True
    counter = defaultdict(int)
    for prev_cave in previous_caves:
        if prev_cave.islower() and prev_cave not in ['start', 'end']:
            counter[prev_cave] += 1
    return all(map(lambda v: v <= 1, counter.values()))


def all_done(list):
    return all(map(lambda l: l[-1] == 'end', list))
