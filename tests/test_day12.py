from pprint import pprint
from aoc.day12 import part1, part2, util

first_example_input = """start-A
start-b
A-c
A-b
b-d
A-end
b-end""".splitlines()

first_example_paths = """start,A,b,A,c,A,end
start,A,b,A,end
start,A,b,end
start,A,c,A,b,A,end
start,A,c,A,b,end
start,A,c,A,end
start,A,end
start,b,A,c,A,end
start,b,A,end
start,b,end""".splitlines()

slightly_larger_example = """dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc""".splitlines()

larger_example = """fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(first_example_input) == 10
    assert part1.result(slightly_larger_example) == 19
    assert part1.result(larger_example) == 226

#
# --- Part Two ---
#

def test_part2():
    assert util.all_paths(first_example_input, alternate_logic=True) == parse_paths(first_example_paths_alternate)
    assert part2.result(first_example_input) == 36
    assert part2.result(slightly_larger_example) == 103
    assert part2.result(larger_example) == 3509

def parse_paths(paths):
    o = list(map(lambda row: row.split(","), paths))
    o.sort()
    return o


first_example_paths_alternate = """start,A,b,A,b,A,c,A,end
start,A,b,A,b,A,end
start,A,b,A,b,end
start,A,b,A,c,A,b,A,end
start,A,b,A,c,A,b,end
start,A,b,A,c,A,c,A,end
start,A,b,A,c,A,end
start,A,b,A,end
start,A,b,d,b,A,c,A,end
start,A,b,d,b,A,end
start,A,b,d,b,end
start,A,b,end
start,A,c,A,b,A,b,A,end
start,A,c,A,b,A,b,end
start,A,c,A,b,A,c,A,end
start,A,c,A,b,A,end
start,A,c,A,b,d,b,A,end
start,A,c,A,b,d,b,end
start,A,c,A,b,end
start,A,c,A,c,A,b,A,end
start,A,c,A,c,A,b,end
start,A,c,A,c,A,end
start,A,c,A,end
start,A,end
start,b,A,b,A,c,A,end
start,b,A,b,A,end
start,b,A,b,end
start,b,A,c,A,b,A,end
start,b,A,c,A,b,end
start,b,A,c,A,c,A,end
start,b,A,c,A,end
start,b,A,end
start,b,d,b,A,c,A,end
start,b,d,b,A,end
start,b,d,b,end
start,b,end""".splitlines()