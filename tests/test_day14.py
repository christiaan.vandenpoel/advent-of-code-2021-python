from aoc.day14 import part1, part2
example_input = """NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(example_input) == 1588

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(example_input) == 2188189693529
