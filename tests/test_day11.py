from aoc.day11 import part1, part2

simple_example_input = """11111
19991
19191
19991
11111""".splitlines()

large_example_input = """5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(simple_example_input, 1) == 9
    assert part1.result(simple_example_input, 2) == 9

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(large_example_input) == 195
