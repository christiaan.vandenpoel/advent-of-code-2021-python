from pprint import pprint

from aoc.day15 import part1, part2
from aoc.day15.util import parse_input, print_cave, expand

example_input = """1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581""".splitlines()


#
# --- Part One ---
#

def test_part1():
    assert part1.result(example_input) == 40


#
# --- Part Two ---
#

def test_part2():
    assert part2.result(example_input) == 315


#
# --- util tests ---
#
def test_expand():
    test_input = """12
34""".splitlines()

    cave = parse_input(test_input)
    expected = """122334
344556
233445
455667
344556
566778""".splitlines()
    assert expand(cave, 3, len(test_input), len(test_input[0])) == parse_input(expected)

    test_input = """1""".splitlines()

    cave = parse_input(test_input)
    expected = """123
234
345""".splitlines()
    assert expand(cave, 3, len(test_input), len(test_input[0])) == parse_input(expected)


    test_input = """8""".splitlines()

    cave = parse_input(test_input)
    print_cave(cave, 1, 1)
    pprint(cave)
    expected = """89123
91234
12345
23456
34567""".splitlines()


    assert expand(cave, 5, 1, 1) == parse_input(expected)


