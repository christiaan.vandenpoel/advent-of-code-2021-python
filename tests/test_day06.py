import pytest

from aoc.day06 import part1, part2

example_input = """3,4,3,1,2""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(example_input, 80) == 5934

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(example_input, 18) == 26
    assert part1.result(example_input, 80) == 5934
    assert part2.result(example_input, 256) == 26984457539

