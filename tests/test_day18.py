import pytest
from aoc.day18 import part1, part2, snailfish


#
# --- Part One ---
#

def test_part1():
    pass
    # assert solve(explode_example_1) == parse("[[[[0,9],2],3],4]")
    assert part1.solve(example_homework2) == snailfish.parse_line("[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]")
    assert part1.result(magnitude_example) == 143
    assert part1.result(example_homework2) == 4140


#
# --- Part Two ---
#

def test_part2():
    assert part2.result(example_homework2) == 3993


explode_example_1 = """[[[[[9,8],1],2],3],4]""".splitlines()

example_homework2 = """[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]""".splitlines()

magnitude_example = """[1,2]
[[3,4],5]""".splitlines()