from aoc.day09 import part1, part2

example_input = """2199943210
3987894921
9856789892
8767896789
9899965678""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(example_input) == 15

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(example_input) == 1134
