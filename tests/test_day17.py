import pytest

from aoc.day17 import part1, part2

example_input = """target area: x=20..30, y=-10..-5""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(example_input) == 45

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(example_input) == 112
