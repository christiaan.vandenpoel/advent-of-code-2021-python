from aoc.day07 import part1, part2

example_input = "16,1,2,0,4,2,7,1,2,14".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(example_input) == 37

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(example_input) == 168
